--Задание 4 скрипт для создания БД
CREATE DATABASE "JBorn_db";


CREATE TABLE person
(
    id       SERIAL PRIMARY KEY,
    email    varchar(255) NOT NULL UNIQUE ,
    password varchar(255) NOT NULL
);

CREATE TABLE account
(
    id           SERIAL PRIMARY KEY,
    title        varchar(255) NOT NULL,
    amount_money int,
    person_id    INT REFERENCES person (id)
);

CREATE TABLE transaction
(
    id              SERIAL PRIMARY KEY,
    to_account_id   int references account (id),
    from_account_id int references account (id),
    transaction_sum int,
    created_date    timestamp
);

CREATE TABLE category
(
    id        SERIAL PRIMARY KEY,
    title     varchar(255) NOT NULL,
    person_id INT REFERENCES person (id)
);

CREATE TABLE transaction_to_category
(
    transaction_id int REFERENCES transaction (id),
    category_id    int REFERENCES category (id)

);

--Задание 5
--1
SELECT a.title, a.amount_money
FROM account as a
WHERE person_id = 2;

--2
SELECT t.created_date,
       t.transaction_sum,
       p.id
FROM transaction as t, account as a
                           JOIN person as p on p.id = a.person_id
WHERE created_date = (current_date - 1) AND p.id = 1 ;

--3
select p.id,
       p.email,
       sum(a.amount_money) as summa
from person as p
         left join account as a on p.id = a.person_id
group by p.email, p.id;