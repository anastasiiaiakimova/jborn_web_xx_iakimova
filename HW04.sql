SELECT c.title, sum(t.transaction_sum) as summa
FROM transaction as t
         JOIN transaction_to_category ttc on t.id = ttc.transaction_id
         JOIN category c on c.id = ttc.category_id
         JOIN account a on a.id = t.to_account_id
WHERE c.person_id = 14 AND a.person_id = 14
  AND t.created_date BETWEEN '2023-03-01 15:29:21.000000' AND
    '2023-03-29 15:29:21.000000'
GROUP BY c.title;

SELECT c.title, sum(t.transaction_sum) as summa
FROM transaction as t
         JOIN transaction_to_category ttc on t.id = ttc.transaction_id
         JOIN category c on c.id = ttc.category_id
         JOIN account a on a.id = t.from_account_id
WHERE c.person_id = 14 AND a.person_id = 14
  AND t.created_date BETWEEN '2023-03-01 15:29:21.000000' AND
    '2023-03-29 15:29:21.000000'
GROUP BY c.title;