package ru.yakimova.security;

public enum UserRole {
    USER,
    ADMIN
}
