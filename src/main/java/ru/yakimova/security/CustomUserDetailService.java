package ru.yakimova.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.person.Person;

import java.util.stream.Collectors;

@Service
@Profile("!test")
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final PersonRepository personRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Person person = personRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Person has not been found"));

        return new CustomUserDetails(
                person.getId(),
                person.getEmail(),
                person.getPassword(),
                person.getRoles()
                        .stream()
                        .map(CustomGrantedAuthority::new)
                        .collect(Collectors.toList())
        );
    }
}
