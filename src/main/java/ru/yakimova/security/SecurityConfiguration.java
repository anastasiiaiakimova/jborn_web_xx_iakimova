package ru.yakimova.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static ru.yakimova.security.UserRole.ADMIN;
import static ru.yakimova.security.UserRole.USER;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        String[] authResourcesWeb = {
                "/person/login-form",
                "/person/registration",
                "/api/registration"
        };

        String[] resourcesArea = {
                "/person/personal-area",
                "/person/account/**",
                "/person/category/**",
                "/person/report/**",
                "/person/transaction/**",
                "/api/**"
        };

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(authResourcesWeb).permitAll()
                .antMatchers(resourcesArea).hasAnyRole(USER.name(), ADMIN.name())
                .and()
                .formLogin()
                .usernameParameter("email")
                .passwordParameter("password")
                .loginPage("/person/login-form")
                .loginProcessingUrl("/person/login")
                .defaultSuccessUrl("/person/personal-area")
                .and()
                .logout()
                .logoutUrl("/person/logout")
                .logoutSuccessUrl("/person/login-form");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
