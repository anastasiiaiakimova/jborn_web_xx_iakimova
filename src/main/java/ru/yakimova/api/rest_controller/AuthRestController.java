package ru.yakimova.api.rest_controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.yakimova.api.json.person.AuthRequest;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.service.person.AuthService;
import ru.yakimova.utils.BaseController;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AuthRestController extends BaseController {
    private final AuthService authService;

    @GetMapping("/person")
    public ResponseEntity<AuthResponse> login() {
        AuthResponse person = authService.getPersonById(currentUser().getId());
        return ok(person);
    }

    @PostMapping("/registration")
    public ResponseEntity<AuthResponse> registration(
            @RequestBody @Valid AuthRequest authRequest) {
        AuthResponse person = authService.registration(authRequest.getEmail(), (authRequest.getPassword()));
        return ok(person);
    }
}

