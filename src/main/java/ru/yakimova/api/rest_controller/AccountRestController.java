package ru.yakimova.api.rest_controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.yakimova.api.json.account.*;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.utils.BaseController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccountRestController extends BaseController {
    private final AccountService service;

    @GetMapping("/find-all-accounts")
    public ResponseEntity<FindAllAccountsResponse> findAllById() {
        List<AccountResponse> responses = service.findAllByPersonId(currentUser().getId());
        return ok(new FindAllAccountsResponse(responses));
    }

    @PostMapping("/create-account")
    public ResponseEntity<AccountResponse> create(
            @RequestBody @Valid AccountRequest accountRequest) {
        AccountResponse account = service.create(currentUser().getId(), accountRequest.getTitle(), accountRequest.getAmountMoney());
        return ok(account);
    }

    @PostMapping("/update-account")
    public ResponseEntity<AccountResponse> update(
            @RequestBody @Valid UpdateAccountRequest accountRequest) {
        AccountResponse account = service.update(currentUser().getId(), accountRequest.getId(), accountRequest.getTitle());
        return ok(account);
    }

    @PostMapping("/delete-account")
    public ResponseEntity<DeleteAccountResponse> delete(
            @RequestBody @Valid DeleteAccountRequest accountRequest) {
        service.delete(currentUser().getId(), accountRequest.getId());
        return ok(new DeleteAccountResponse("Account " + accountRequest.getId() + " has been deleted"));
    }
}
