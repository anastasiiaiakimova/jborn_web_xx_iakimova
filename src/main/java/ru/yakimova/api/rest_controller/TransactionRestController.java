package ru.yakimova.api.rest_controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.yakimova.api.json.transaction.TransactionRequest;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.service.transaction.TransactionService;
import ru.yakimova.utils.BaseController;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TransactionRestController extends BaseController {
    private final TransactionService transactionService;

    @PostMapping("/create-transaction")
    public ResponseEntity<TransactionResponse> create(
            @RequestBody @Valid TransactionRequest request) {
        TransactionResponse transactional = transactionService.create(
                currentUser().getId(),
                request.getToAccountId(),
                request.getFromAccountId(),
                request.getTransactionSum(),
                request.getCategoriesId()
        );
        return ok(transactional);
    }
}
