package ru.yakimova.api.rest_controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.yakimova.api.json.category.*;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.utils.BaseController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class CategoryRestController extends BaseController {
    private final CategoryService service;

    @GetMapping("/find-all-categories")
    public ResponseEntity<FindAllCategoryResponse> findAllCategoryResponse() {
        List<CategoryResponse> categoryResponses = service.findAllByPersonId(currentUser().getId());
        return ok(new FindAllCategoryResponse(categoryResponses));
    }

    @PostMapping("/create-category")
    public ResponseEntity<CategoryResponse> create(
            @RequestBody @Valid CategoryRequest categoryRequest) {
        CategoryResponse category = service.create(categoryRequest.getTitle(), currentUser().getId());
        return ok(category);
    }

    @PostMapping("/update-category")
    public ResponseEntity<CategoryResponse> update(
            @RequestBody @Valid UpdateCategoryRequest categoryRequest) {
        CategoryResponse category = service.update(categoryRequest.getId(), categoryRequest.getTitle(), currentUser().getId());
        return ok(category);
    }

    @PostMapping("/delete-category")
    public ResponseEntity<DeleteCategoryResponse> delete(
            @RequestBody @Valid DeleteCategoryRequest categoryRequest) {
        service.delete(categoryRequest.getId(), currentUser().getId());
        return ok(new DeleteCategoryResponse("Category " + categoryRequest.getId() + " has been deleted"));
    }
}
