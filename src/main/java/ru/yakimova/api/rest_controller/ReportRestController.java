package ru.yakimova.api.rest_controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.yakimova.api.json.report.AllReportResponse;
import ru.yakimova.api.json.report.ReportRequest;
import ru.yakimova.api.json.report.ReportResponse;
import ru.yakimova.service.report.ReportService;
import ru.yakimova.utils.BaseController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ReportRestController extends BaseController {
    private final ReportService reportService;

    @PostMapping("all-report")
    public ResponseEntity<AllReportResponse> findAllReport(
            @RequestBody @Valid ReportRequest reportRequest) {
        Integer personId = currentUser().getId();
        LocalDateTime start = reportRequest.getStartDate();
        LocalDateTime end = reportRequest.getEndDate();
        Map<String, Integer> reportEarn = reportService.findEarnTransactionToDate(personId, start, end);
        Map<String, Integer> reportSpent = reportService.findSpentTransactionToDate(personId, start, end);
        if (reportEarn.isEmpty() && reportSpent.isEmpty()) {
            return status(HttpStatus.NO_CONTENT).build();
        }
        return ok(new AllReportResponse(reportEarn, reportSpent));
    }

    @PostMapping("earn-report")
    public ResponseEntity<ReportResponse> findEarnReport(
            @RequestBody @Valid ReportRequest reportRequest) {
        Map<String, Integer> reportEarn = reportService.findEarnTransactionToDate(
                currentUser().getId(),
                reportRequest.getStartDate(),
                reportRequest.getEndDate()
        );
        if (reportEarn.isEmpty()) {
            return status(HttpStatus.NO_CONTENT).build();
        }
        return ok(new ReportResponse(reportEarn));
    }

    @PostMapping("spent-report")
    public ResponseEntity<ReportResponse> findSpentReport(
            @RequestBody @Valid ReportRequest reportRequest) {
        Map<String, Integer> reportSpent = reportService.findSpentTransactionToDate(
                currentUser().getId(),
                reportRequest.getStartDate(),
                reportRequest.getEndDate()
        );
        if (reportSpent.isEmpty()) {
            return status(HttpStatus.NO_CONTENT).build();
        }
        return ok(new ReportResponse(reportSpent));
    }
}


