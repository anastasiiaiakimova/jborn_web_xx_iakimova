package ru.yakimova.api.json.person;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class AuthRequest {

    @Email(message = "Email is not valid")
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;
}
