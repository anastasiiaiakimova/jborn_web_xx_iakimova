package ru.yakimova.api.json.category;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryResponse {
    private int id;
    private String title;
    private int personId;
}
