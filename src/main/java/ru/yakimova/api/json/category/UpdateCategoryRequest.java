package ru.yakimova.api.json.category;

import lombok.Data;

@Data
public class UpdateCategoryRequest {
    private int id;
    private String title;
}
