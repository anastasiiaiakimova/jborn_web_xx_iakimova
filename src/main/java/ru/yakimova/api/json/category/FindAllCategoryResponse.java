package ru.yakimova.api.json.category;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FindAllCategoryResponse {
    private List<CategoryResponse> categories;
}
