package ru.yakimova.api.json.category;

import lombok.Data;

@Data
public class CategoryRequest {
    private String title;
}
