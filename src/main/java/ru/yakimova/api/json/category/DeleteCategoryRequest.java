package ru.yakimova.api.json.category;

import lombok.Data;

@Data
public class DeleteCategoryRequest {
    private int id;
}
