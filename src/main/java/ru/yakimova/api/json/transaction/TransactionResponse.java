package ru.yakimova.api.json.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TransactionResponse {
    private int id;
    private int toAccountId;
    private int fromAccountId;
    private int transactionSum;
    private LocalDateTime createdDate;

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", toAccountId=" + toAccountId +
                ", fromAccountId=" + fromAccountId +
                ", transactionSum=" + transactionSum +
                ", createdDate=" + createdDate +
                '}';
    }
}
