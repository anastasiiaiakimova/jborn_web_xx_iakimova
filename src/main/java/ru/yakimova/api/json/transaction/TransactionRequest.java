package ru.yakimova.api.json.transaction;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TransactionRequest {

    @Min(value = 1)
    private int toAccountId;

    @Min(value = 1)
    private int fromAccountId;

    @Min(value = 10)
    private int transactionSum;

    @NotNull
    private List<Integer> categoriesId;
}
