package ru.yakimova.api.json.account;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class DeleteAccountRequest {

    @Min(value = 1)
    private int id;
}
