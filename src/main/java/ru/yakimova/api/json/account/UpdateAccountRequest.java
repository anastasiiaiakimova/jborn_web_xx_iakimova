package ru.yakimova.api.json.account;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UpdateAccountRequest {

    @Min(value = 1)
    private int id;

    @NotEmpty
    @Size(min = 2, max = 40)
    private String title;

    @Min(value = 0)
    private int amountMoney;
}
