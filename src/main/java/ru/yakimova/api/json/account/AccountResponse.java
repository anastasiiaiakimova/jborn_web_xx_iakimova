package ru.yakimova.api.json.account;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountResponse {
    private int id;
    private String title;
    private int amountMoney;
    private int personId;

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", amountMoney=" + amountMoney +
                ", personId=" + personId +
                '}';
    }
}
