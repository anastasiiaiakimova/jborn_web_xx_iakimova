package ru.yakimova.api.json.account;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class AccountRequest {

    @NotEmpty
    private String title;

    @Min(value = 0)
    private int amountMoney;
}


