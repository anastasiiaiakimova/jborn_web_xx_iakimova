package ru.yakimova.api.json.account;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FindAllAccountsResponse {
    private List<AccountResponse> accounts;
}
