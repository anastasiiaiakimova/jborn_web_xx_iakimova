package ru.yakimova.api.json.report;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class AllReportResponse {
    private Map<String, Integer> reportEarn;
    private Map<String, Integer> reportSpent;
}
