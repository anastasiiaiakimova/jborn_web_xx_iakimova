package ru.yakimova.api.json.report;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class ReportResponse {
    private Map<String, Integer> report;
}
