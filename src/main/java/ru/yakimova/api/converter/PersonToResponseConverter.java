package ru.yakimova.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.yakimova.dao.person.Person;
import ru.yakimova.api.json.person.AuthResponse;

@Component
public class PersonToResponseConverter implements Converter<Person, AuthResponse> {

    @Override
    public AuthResponse convert(Person source) {
        return new AuthResponse(source.getId(), source.getEmail());
    }
}
