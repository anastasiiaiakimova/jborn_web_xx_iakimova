package ru.yakimova.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.dao.transaction.Transaction;

@Component
public class TransactionToResponseConverter implements Converter<Transaction, TransactionResponse> {

    @Override
    public TransactionResponse convert(Transaction source) {
        return new TransactionResponse(
                source.getId(),
                source.getToAccount().getId(),
                source.getFromAccount().getId(),
                source.getTransactionSum(),
                source.getCreatedDate());
    }
}


