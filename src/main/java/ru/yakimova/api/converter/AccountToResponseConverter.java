package ru.yakimova.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.dao.account.Account;

@Component
public class AccountToResponseConverter implements Converter<Account, AccountResponse> {

    @Override
    public AccountResponse convert(Account source) {
        return new AccountResponse(source.getId(), source.getTitle(), source.getAmountMoney(), source.getPerson().getId());
    }
}


