package ru.yakimova.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.dao.category.Category;

@Component
public class CategoryToResponseConverter implements Converter<Category, CategoryResponse> {

    @Override
    public CategoryResponse convert(Category source) {
        return new CategoryResponse(source.getId(), source.getTitle(), source.getPerson().getId());
    }
}


