package ru.yakimova.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yakimova.dao.account.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    List<Account> findAccountByPersonId(Integer personId);

    Optional<Account> findAccountByIdAndPersonId(Integer id, Integer personId);
}
