package ru.yakimova.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yakimova.dao.transaction.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
}
