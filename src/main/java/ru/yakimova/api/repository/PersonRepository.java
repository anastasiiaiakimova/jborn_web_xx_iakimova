package ru.yakimova.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yakimova.dao.person.Person;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    Optional<Person> findByEmail(String email);

    Person findByEmailAndPassword(String email, String password);
}
