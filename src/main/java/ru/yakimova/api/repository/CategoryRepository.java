package ru.yakimova.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yakimova.dao.category.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    List<Category> findCategoriesByPersonId(Integer personId);

    Optional<Category> findCategoryByIdAndPersonId(Integer id, Integer personId);
}
