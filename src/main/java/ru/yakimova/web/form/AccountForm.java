package ru.yakimova.web.form;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class AccountForm {

    @NotEmpty
    private String title;

    @Min(value = 0)
    private int amountMoney;
}
