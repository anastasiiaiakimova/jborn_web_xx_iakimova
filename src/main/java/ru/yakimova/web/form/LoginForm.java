package ru.yakimova.web.form;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginForm {

    @Email(message = "Email is not valid")
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;
}
