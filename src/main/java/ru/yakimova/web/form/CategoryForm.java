package ru.yakimova.web.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CategoryForm {

    @NotEmpty
    private String title;
}
