package ru.yakimova.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.exception.CustomException;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.service.transaction.TransactionService;
import ru.yakimova.utils.BaseController;
import ru.yakimova.web.form.TransactionForm;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/transaction")
public class TransactionController extends BaseController {
    private final TransactionService transactionService;
    private final AccountService accountService;
    private final CategoryService categoryService;

    @GetMapping("/create-transaction")
    public String createForm(Model model) {
        Integer personId = currentUser().getId();
        model.addAttribute("accounts", accountService.findAllByPersonId(personId));
        model.addAttribute("categories", categoryService.findAllByPersonId(personId));
        model.addAttribute("form", new TransactionForm());
        return "/transaction/create-transaction";
    }

    @PostMapping("/create-transaction")
    public String createTransaction(
            @ModelAttribute("transactionForm") @Valid TransactionForm form,
            BindingResult result,
            Model model) {
        Integer personId = currentUser().getId();
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        }
        TransactionResponse transactional = transactionService.create(personId, form.getToAccountId(), form.getFromAccountId(), form.getTransactionSum(), form.getCategoriesId());
        model.addAttribute("transactional", transactional);
        model.addAttribute("accountFrom", accountService.findById(transactional.getFromAccountId(), personId).getTitle());
        model.addAttribute("accountTo", accountService.findById(transactional.getToAccountId(), personId).getTitle());
        List<CategoryResponse> categoryResponseList = (form.getCategoriesId()).stream()
                .map(x -> categoryService.findById(x, personId))
                .collect(Collectors.toList());
        model.addAttribute("categories", categoryResponseList);
        model.addAttribute("form", form);
        return "/transaction/transaction-show";
    }
}
