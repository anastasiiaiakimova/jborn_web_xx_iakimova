package ru.yakimova.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.exception.CustomException;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.utils.BaseController;
import ru.yakimova.web.form.CategoryForm;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/category")
public class CategoryController extends BaseController {
    private final CategoryService categoryService;

    @GetMapping("/find-all-categories")
    public String index(Model model) {
        List<CategoryResponse> categories = categoryService.findAllByPersonId(currentUser().getId());
        model.addAttribute("categories", categories);
        return "/category/categories";
    }

    @GetMapping("/create-category")
    public String createForm(Model model) {
        model.addAttribute("form", new CategoryForm());
        return "/category/create-category";
    }

    @PostMapping("/create-category")
    public String reportRequest(
            @ModelAttribute("categoryForm") @Valid CategoryForm form,
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        } else {
            CategoryResponse category = categoryService.create(form.getTitle(), currentUser().getId());
            model.addAttribute("title", category.getTitle());
            model.addAttribute("form", form);
        }
        return "redirect:/category/find-all-categories";
    }

    @GetMapping("/{categoryId}")
    public String getCategoryById(Model model, @PathVariable Integer categoryId) {
        CategoryResponse category = categoryService.findById(categoryId, currentUser().getId());
        model.addAttribute("allowDelete", false);
        model.addAttribute("category", category);
        return "/category/categoryId";
    }

    @GetMapping("/{categoryId}/edit")
    public String showEditCategory(Model model, @PathVariable Integer categoryId) {
        CategoryResponse category = categoryService.findById(categoryId, currentUser().getId());
        model.addAttribute("category", category);
        return "/category/edit-category";
    }

    @PostMapping("/{categoryId}/edit")
    public String editCategory(
            @ModelAttribute("categoryForm") @Valid CategoryForm form,
            @PathVariable Integer categoryId,
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        } else {
            CategoryResponse category = categoryService.update(categoryId, form.getTitle(), currentUser().getId());
            model.addAttribute("category", category);
            return "redirect:/category/find-all-categories";
        }
    }

    @GetMapping("/{categoryId}/delete")
    public String deleteCategory(
            @PathVariable Integer categoryId) {
        categoryService.delete(categoryId, currentUser().getId());
        return "redirect:/category/find-all-categories";
    }
}
