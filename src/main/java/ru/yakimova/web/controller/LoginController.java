package ru.yakimova.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.exception.CustomException;
import ru.yakimova.service.person.AuthService;
import ru.yakimova.utils.BaseController;
import ru.yakimova.web.form.LoginForm;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/person")
public class LoginController extends BaseController {
    private final AuthService authService;

    @GetMapping(value = "/personal-area")
    public String index(Model model) {
        AuthResponse person = authService.getPersonById(currentUser().getId());
        model.addAttribute("id", person.getId())
                .addAttribute("email", person.getEmail());
        return "/person/personal-area";
    }

    @GetMapping("/login")
    public String getLogin() {
        return "/person/login-form";
    }

    @GetMapping("/registration")
    public String getRegistration(Model model) {
        model.addAttribute("form", new LoginForm());
        return "/person/registration";
    }

    @PostMapping("/registration")
    public String postRegistration(
            @ModelAttribute("form") @Valid LoginForm form,
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        } else {
            AuthResponse person = authService.registration(form.getEmail(), form.getPassword());
            if (person == null) {
                throw new CustomException(result.toString());
            }
            model.addAttribute("form", new LoginForm());
            return "redirect:/person/login";
        }
    }

    @GetMapping("login-form")
    public String getLogin(Model model, HttpServletRequest request) {
        if (request.getParameterMap().containsKey("error")) {
            model.addAttribute("errorMessage", true);
        }
        return "/person/login-form";
    }
}
