package ru.yakimova.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.exception.CustomException;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.utils.BaseController;
import ru.yakimova.web.form.AccountForm;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/account")
public class AccountController extends BaseController {
    private final AccountService service;

    @GetMapping("/find-all-accounts")
    public String index(Model model) {
        List<AccountResponse> accounts = service.findAllByPersonId(currentUser().getId());
        model.addAttribute("accounts", accounts);
        return "/account/accounts";
    }

    @GetMapping("/create-account")
    public String createForm(Model model) {
        model.addAttribute("form", new AccountForm());
        return "/account/create-account";
    }

    @PostMapping("/create-account")
    public String reportRequest(
            @ModelAttribute("accountForm") @Valid AccountForm form,
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        } else {
            service.create(currentUser().getId(), form.getTitle(), form.getAmountMoney());
        }
        return "redirect:/account/find-all-accounts";
    }

    @GetMapping("/{accountId}")
    public String getAccountById(
            Model model,
            @PathVariable Integer accountId) {
        AccountResponse account = service.findById(accountId, currentUser().getId());
        model.addAttribute("allowDelete", false);
        model.addAttribute("account", account);
        return "/account/accountId";
    }

    @GetMapping("/{accountId}/edit")
    public String showEditAccount(
            Model model,
            @PathVariable Integer accountId) {
        AccountResponse account = service.findById(accountId, currentUser().getId());
        model.addAttribute("account", account);
        return "/account/edit-account";
    }

    @PostMapping("/{accountId}/edit")
    public String editAccount(
            @ModelAttribute("accountForm") @Valid AccountForm form,
            BindingResult result,
            @PathVariable Integer accountId,
            Model model) {
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        } else {
            AccountResponse account = service.update(currentUser().getId(), accountId, form.getTitle());
            model.addAttribute("account", account);
            return "redirect:/account/find-all-accounts";
        }
    }

    @GetMapping("/{accountId}/delete")
    public String deleteAccount(@PathVariable Integer accountId) {
        service.delete(currentUser().getId(), accountId);
        return "redirect:/account/find-all-accounts";
    }
}
