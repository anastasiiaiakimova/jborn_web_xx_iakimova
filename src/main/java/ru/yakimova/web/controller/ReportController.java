package ru.yakimova.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.yakimova.exception.CustomException;
import ru.yakimova.service.report.ReportService;
import ru.yakimova.utils.BaseController;
import ru.yakimova.web.form.ReportForm;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@RequestMapping("/report")
public class ReportController extends BaseController {
    private final ReportService reportService;

    @PostMapping("/create-report")
    public String reportRequest(
            @ModelAttribute("reportForm") @Valid ReportForm form,
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            throw new CustomException(result.toString());
        } else {
            Integer personId = currentUser().getId();
            LocalDateTime start = form.getStartDate();
            LocalDateTime end = form.getEndDate();
            Map<String, Integer> reportEarn = reportService.findEarnTransactionToDate(personId, start, end);
            Map<String, Integer> reportSpent = reportService.findSpentTransactionToDate(personId, start, end);
            model.addAttribute("reportEarn", reportEarn);
            model.addAttribute("reportSpent", reportSpent);
            model.addAttribute("form", form);
        }
        return "/report/all-report";
    }

    @GetMapping("/create-report")
    public String reportDate(Model model) {
        model.addAttribute("form", new ReportForm());
        return "/report/create-report";
    }
}
