package ru.yakimova;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.exception.CustomException;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.service.person.AuthService;
import ru.yakimova.service.report.ReportService;
import ru.yakimova.service.transaction.TransactionService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
@Profile(value = "!test")
@RequiredArgsConstructor
public class CommandLineRunnerImpl implements CommandLineRunner {
    private final AuthService authService;
    private final AccountService accountService;
    private final CategoryService categoryService;
    private final ReportService reportService;
    private final TransactionService transactionService;
    private AuthResponse person;

    static String request(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.next();
    }

    @Override
    public void run(String... args) {
        String active = request("Выберите действие: 1 " +
                "- авторизация, 2 - регистрация");
        if (checkedActive(active)) {
            System.out.println("Ошибка");
            return;
        }
        System.out.println("Вы выбрали - " + active);
        String email = request("Введите email");
        String password = request("Введите password");
        switch (Integer.parseInt(active)) {
            case 1: {
                person = authService.auth(email, password);
                if (person != null) {
                    personalArea();
                } else {
                    System.out.println("Пользователь не найден!");
                }
                break;
            }
            case 2: {
                person = authService.registration(email, password);
                if (person != null) {
                    System.out.println("Добро пожаловать, " + person.getEmail() + "!");
                } else {
                    System.out.println("Ошибка");
                }
                break;
            }
            default: {
                System.out.println("неверная команда");
                break;
            }
        }
    }

    private void personalArea() {
        String act = (request("Выберите : 1 " +
                "- счета, 2 - категории транзакций, 3 - отчеты за период, 4 - создание транзакции"));
        if (checkedActive(act)) {
            System.out.println("неверная команда");
            return;
        }
        switch (Integer.parseInt(act)) {
            case 1: {
                String activeWithAccountChecked = (
                        request("1 - вывести список счетов; " +
                                "\n2 -  создать счет; " +
                                "\n3 -  удалить счет"));
                if (checkedActive(activeWithAccountChecked)) {
                    System.out.println("Неизвестное действие");
                    return;
                }
                int activeWithAccount = Integer.parseInt(activeWithAccountChecked);
                if (checkedChoseInto(activeWithAccount)) {
                    System.out.println("Неизвестная команда");
                    return;
                }
                if (activeWithAccount == 1) {
                    accountService.findAllByPersonId(person.getId())
                            .forEach(System.out::println);
                }
                if (activeWithAccount == 2) {
                    String title = request("Введите имя нового счета: ");
                    String amountMoneyStr = request("Введите начальную сумму: ");
                    if (checkedActive(amountMoneyStr)) {
                        System.out.println("ошибка");
                        return;
                    }
                    int amountMoney = Integer.parseInt(amountMoneyStr);
                    AccountResponse accountResponse = accountService.create(person.getId(), title, amountMoney);
                    System.out.println(accountResponse);
                }
                if (activeWithAccount == 3) {
                    String idStr = request("Введите id счета для удаления: ");
                    if (checkedActive(idStr)) {
                        System.out.println("ошибка");
                        return;
                    }
                    int id = Integer.parseInt(idStr);
                    accountService.delete(person.getId(), id);
                }
                break;
            }
            case 2: {
                String activeWithCategoryChecked = request("1 - создать тип транзакции; " +
                        "\n2 -  изменить существующую; " +
                        "\n3 -  удалить тип транзакции");
                if (checkedActive(activeWithCategoryChecked)) {
                    System.out.println("Ошибка!");
                    return;
                }
                int activeWithCategory = Integer.parseInt(activeWithCategoryChecked);
                if (checkedChoseInto(activeWithCategory)) {
                    System.out.println("Неизвестная команда");
                    return;
                }
                if (activeWithCategory == 1) {
                    String title = request("Введите имя новой категории: ");
                    CategoryResponse category = categoryService.create(title, person.getId());
                    System.out.println(category);
                }
                if (activeWithCategory == 2) {
                    String checkedId = request("Введите id категории для " +
                            "изменения : ");
                    if (checkedActive(checkedId)) {
                        System.out.println("Ошибка");
                        return;
                    }
                    int id = Integer.parseInt(checkedId);
                    String title = request("Введите новое имя категории: ");
                    CategoryResponse categoryResponse = categoryService.update(id, title, person.getId());
                    System.out.println(categoryResponse);
                }
                if (activeWithCategory == 3) {
                    String checkedId = request("Введите id типа транзакции " +
                            "для удаления: ");
                    if (checkedActive(checkedId)) {
                        System.out.println("Ошибка");
                        return;
                    }
                    int id = Integer.parseInt(checkedId);
                    categoryService.delete(id, person.getId());
                }
                break;
            }
            case 3: {
                String activeWithReportChecked = request("1 - показать доход за период; " +
                        "\n2 -  показать расход за период;" +
                        "\n3 -  показать доход и расход за период");
                if (checkedActive(activeWithReportChecked)) {
                    System.out.println("Ошибка!");
                    return;
                }
                int activeWithReport = Integer.parseInt(activeWithReportChecked);
                if (checkedChoseInto(activeWithReport)) {
                    System.out.println("Неизвестная команда");
                    return;
                }
                if (activeWithReport == 1) {
                    LocalDateTime[] dates = changeAndCheckedValidDate();
                    if (!Optional.ofNullable(dates).isPresent()) {
                        return;
                    }
                    LocalDateTime startDate = dates[0];
                    LocalDateTime endDate = dates[1];
                    Map<String, Integer> report = reportService
                            .findEarnTransactionToDate(person.getId(), dates[0], dates[1]);
                    System.out.println("Доход за период c " + startDate + " по " + endDate + ":");
                    report.forEach((key, value) -> System.out.println(key + " : " + value));
                }
                if (activeWithReport == 2) {
                    LocalDateTime[] dates = changeAndCheckedValidDate();
                    if (!Optional.ofNullable(dates).isPresent()) {
                        return;
                    }
                    LocalDateTime startDate = dates[0];
                    LocalDateTime endDate = dates[1];
                    Map<String, Integer> report = reportService
                            .findSpentTransactionToDate(person.getId(), startDate, endDate);
                    System.out.println("Расход за период c " + startDate + " по " + endDate + ":");
                    report.forEach((key, value) -> System.out.println(key + " : " + value));
                }
                if (activeWithReport == 3) {
                    LocalDateTime[] dates = changeAndCheckedValidDate();
                    if (!Optional.ofNullable(dates).isPresent()) {
                        return;
                    }
                    LocalDateTime startDate = dates[0];
                    LocalDateTime endDate = dates[1];
                    Map<String, Integer> report = reportService
                            .findEarnTransactionToDate(person.getId(), startDate, endDate);
                    System.out.println("Доход за период c " + startDate + " по " + endDate + ":");
                    report.forEach((key, value) -> System.out.println(key + " : " + value));
                    Map<String, Integer> report2 = reportService
                            .findSpentTransactionToDate(person.getId(), startDate, endDate);
                    System.out.println("Расход за период c " + startDate + " по " + endDate + ":");
                    report2.forEach((key, value) -> System.out.println(key + " : " + value));
                }
                break;
            }
            case 4: {
                System.out.println("Создание транзакции");
                String toAccountIdStr = request("Введите id счета получателя:");
                if (checkedActive(toAccountIdStr)) {
                    System.out.println("Ошибка!");
                    return;
                }
                int toAccountId = Integer.parseInt(toAccountIdStr);
                String fromAccountIdStr = request("Введите id счета списания:");
                if (checkedActive(fromAccountIdStr)) {
                    System.out.println("Ошибка!");
                    return;
                }
                int fromAccountId = Integer.parseInt(fromAccountIdStr);
                String transactionSumStr = request("Введите сумму:");
                if (checkedActive(transactionSumStr)) {
                    System.out.println("Ошибка!");
                    return;
                }
                int transactionSum = Integer.parseInt(transactionSumStr);
                List<Integer> categoryId = new ArrayList<>();
                System.out.println("Введите id категории:");
                Scanner scan = new Scanner(System.in);
                int listCategoryId = -1;
                while (listCategoryId != 0) {
                    listCategoryId = scan.nextInt();
                    if (listCategoryId != 0) {
                        categoryId.add(listCategoryId);
                        System.out.println("Введите id категории или 0 для завершения");
                    }
                }
                try {
                    TransactionResponse transaction = transactionService.create(
                            person.getId(),
                            toAccountId,
                            fromAccountId,
                            transactionSum,
                            categoryId);

                    System.out.println(transaction);
                } catch (CustomException e) {
                    e.printStackTrace();
                    System.out.println("Транзакция не создана");
                }
                break;
            }
            default: {
                System.out.println("Неизвестное действие");
                break;
            }
        }
    }

    public static boolean checkedActive(String activeForChecked) {
        return !activeForChecked.matches("\\d+");
    }

    public static boolean checkedChoseInto(int checkedValue) {
        return !((checkedValue == 1) || (checkedValue == 2) || (checkedValue == 3));
    }

    public static LocalDateTime[] changeAndCheckedValidDate() {
        Scanner scannerDate = new Scanner(System.in);
        System.out.println("Введите начальную дату, формат YYYY-MM-DD hh:mm:ss");
        String startDateStr = scannerDate.nextLine();
        String pattern = "\\d{4}\\-\\d{2}\\-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(startDateStr);
        if (!m.matches()) {
            System.out.println("Error start date");
            return null;
        }
        System.out.println("Введите конечную дату, формат YYYY-MM-DD hh:mm:ss");
        String endDateStr = scannerDate.nextLine();
        m = p.matcher(endDateStr);
        if (!m.matches()) {
            System.out.println("Error end date");
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDate = LocalDateTime.parse(startDateStr, formatter);
        LocalDateTime endDate = LocalDateTime.parse(endDateStr, formatter);
        return new LocalDateTime[]{startDate, endDate};
    }
}
