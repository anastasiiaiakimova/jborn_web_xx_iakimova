package ru.yakimova.service.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yakimova.api.converter.TransactionToResponseConverter;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.api.repository.AccountRepository;
import ru.yakimova.api.repository.CategoryRepository;
import ru.yakimova.api.repository.TransactionRepository;
import ru.yakimova.dao.account.Account;
import ru.yakimova.dao.category.Category;
import ru.yakimova.dao.transaction.Transaction;
import ru.yakimova.exception.CustomException;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private final AccountRepository accountRepository;
    private final CategoryRepository categoryRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionToResponseConverter transactionToResponseConverter;

    @Transactional(rollbackOn = Exception.class)
    public TransactionResponse create(int personId, int toAccountId, int fromAccountId, int transactionSum, List<Integer> categoryId) {

        Transaction transaction = new Transaction();

        transaction.setCreatedDate(LocalDateTime.now());
        transaction.setTransactionSum(transactionSum);
        Account accountFrom = accountRepository
                .findAccountByIdAndPersonId(fromAccountId, personId)
                .orElseThrow(() -> new CustomException("AccountFrom has not been found"));

        if (accountFrom.getAmountMoney() - transactionSum < 0) {
            throw new CustomException("Sum is too much");
        }
        accountFrom.setAmountMoney(accountFrom.getAmountMoney() - transactionSum);
        transaction.setFromAccount(accountFrom);

        Account accountTo = accountRepository
                .findAccountByIdAndPersonId(toAccountId, personId)
                .orElseThrow(() -> new CustomException("AccountFrom has not been found"));
        accountTo.setAmountMoney(accountTo.getAmountMoney() + transactionSum);
        transaction.setToAccount(accountTo);

        List<Category> categories = new ArrayList<>();
        for (Integer id : categoryId) {
            Category category = categoryRepository
                    .findCategoryByIdAndPersonId(id, personId)
                    .orElseThrow(() -> new CustomException("Categories have not belonged person"));
            categories.add(category);
        }
        if (categories.isEmpty()) {
            throw new CustomException("Categories have not been found");
        }
        transaction.setCategories(categories);
        accountFrom.getTransactionSpend().add(transaction);
        accountTo.getTransactionEarn().add(transaction);
        transactionRepository.save(transaction);
        return transactionToResponseConverter.convert(transaction);
    }
}
