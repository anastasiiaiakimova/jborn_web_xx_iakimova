package ru.yakimova.service.transaction;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class TransactionDTO {

    private int id;
    private int toAccountId;
    private int fromAccountId;
    private int transactionSum;
    private LocalDateTime createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionDTO that = (TransactionDTO) o;
        return id == that.id && toAccountId == that.toAccountId && fromAccountId == that.fromAccountId && transactionSum == that.transactionSum && Objects.equals(createdDate, that.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, toAccountId, fromAccountId, transactionSum, createdDate);
    }
}
