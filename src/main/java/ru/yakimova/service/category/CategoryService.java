package ru.yakimova.service.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yakimova.api.converter.CategoryToResponseConverter;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.api.repository.CategoryRepository;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.category.Category;
import ru.yakimova.dao.person.Person;
import ru.yakimova.exception.CustomException;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryToResponseConverter categoryToResponseConverter;
    private final PersonRepository personRepository;

    @Transactional
    public CategoryResponse create(String title, int personId) {
        Category category = new Category();
        category.setTitle(title);
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new CustomException("Person has not been found"));
        category.setPerson(person);
        categoryRepository.save(category);
        return categoryToResponseConverter.convert(category);
    }

    @Transactional
    public CategoryResponse update(int id, String title, int personId) {
        Category category = categoryRepository.findCategoryByIdAndPersonId(id, personId)
                .orElseThrow(() -> new CustomException("Category has not been found"));
        category.setTitle(title);
        return categoryToResponseConverter.convert(category);
    }

    @Transactional
    public void delete(int id, int personId) {
        Category category = categoryRepository.findCategoryByIdAndPersonId(id, personId)
                .orElseThrow(() -> new CustomException("Category has not been found"));
        categoryRepository.delete(category);
    }

    public List<CategoryResponse> findAllByPersonId(int personId) {
        return categoryRepository.findCategoriesByPersonId(personId).stream()
                .map(categoryToResponseConverter::convert)
                .collect(toList());
    }

    public CategoryResponse findById(int id, int personId) {
        Category category = categoryRepository.findCategoryByIdAndPersonId(id, personId)
                .orElseThrow(() -> new CustomException("Category has not been found"));
        return categoryToResponseConverter.convert(category);
    }
}
