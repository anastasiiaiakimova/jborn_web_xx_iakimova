package ru.yakimova.service.category;

import lombok.Data;

import java.util.Objects;

@Data
public class CategoryDTO {

    private int id;
    private String title;
    private int personId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDTO that = (CategoryDTO) o;
        return id == that.id && personId == that.personId && Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, personId);
    }
}
