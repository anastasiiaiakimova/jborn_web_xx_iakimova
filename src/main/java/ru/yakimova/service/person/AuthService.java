package ru.yakimova.service.person;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.yakimova.api.converter.PersonToResponseConverter;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.person.Person;
import ru.yakimova.exception.CustomException;
import ru.yakimova.security.UserRole;

import javax.transaction.Transactional;
import java.util.Optional;

import static java.util.Collections.singleton;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final PersonRepository personRepository;
    private final PasswordEncoder passwordEncoder;
    private final PersonToResponseConverter personToResponseConverter;

    public AuthResponse getPersonById(Integer id) {
        return personRepository.findById(id)
                .map(personToResponseConverter::convert)
                .orElseThrow(() -> new CustomException("Person has not been found"));
    }

    public AuthResponse auth(String email) {
        return personRepository.findByEmail(email)
                .map(personToResponseConverter::convert)
                .orElseThrow(() -> new CustomException("Person has not been found"));
    }

    public AuthResponse auth(String email, String password) {
        Person person = personRepository.findById(auth(email).getId())
                .orElseThrow(() -> new CustomException("Person has not been found"));
        boolean matches = passwordEncoder.matches(password, person.getPassword());
        if (matches) {
            return personToResponseConverter.convert(person);
        } else {
            throw new CustomException("Password is not valid");
        }
    }

    @Transactional
    public AuthResponse registration(String email, String password) {
        Person person = new Person();
        person.setEmail(email);
        String hash = passwordEncoder.encode(password);
        person.setPassword(hash);
        person.setRoles(singleton(UserRole.USER));
        personRepository.save(person);
        return Optional.ofNullable(person)
                .map(personToResponseConverter::convert)
                .orElseThrow(() -> new CustomException("Person has not been registered"));
    }
}
