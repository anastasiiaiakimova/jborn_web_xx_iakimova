package ru.yakimova.service.person;

import lombok.Data;

import java.util.Objects;

@Data
public class PersonDTO {

    private int id;
    private String email;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return id == personDTO.id && Objects.equals(email, personDTO.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }
}
