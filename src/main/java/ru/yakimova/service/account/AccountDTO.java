package ru.yakimova.service.account;

import lombok.Data;

import java.util.Objects;

@Data
public class AccountDTO {

    private int id;
    private String title;
    private int amountMoney;
    private int personId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return id == that.id && amountMoney == that.amountMoney && personId == that.personId && Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, amountMoney, personId);
    }
}
