package ru.yakimova.service.account;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yakimova.api.converter.AccountToResponseConverter;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.api.repository.AccountRepository;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.account.Account;
import ru.yakimova.dao.person.Person;
import ru.yakimova.exception.CustomException;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final PersonRepository personRepository;
    private final AccountToResponseConverter accountToResponseConverter;

    public List<AccountResponse> findAllByPersonId(int personId) {
        return accountRepository.findAccountByPersonId(personId).stream()
                .map(accountToResponseConverter::convert)
                .collect(toList());
    }

    @Transactional
    public AccountResponse create(Integer personId, String title, int amountMoney) {
        Account account = new Account();
        account.setTitle(title);
        account.setAmountMoney(amountMoney);
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new CustomException("Person has not been found"));
        account.setPerson(person);
        Account createdAccount = accountRepository.save(account);
        return accountToResponseConverter.convert(createdAccount);
    }

    @Transactional
    public void delete(int personId, int id) {
        Account account = accountRepository.findAccountByIdAndPersonId(id, personId)
                .orElseThrow(() -> new CustomException("Account has not been found"));
        accountRepository.delete(account);
    }

    @Transactional
    public AccountResponse update(int personId, int id, String newTitle) {
        Account account = accountRepository.findAccountByIdAndPersonId(id, personId)
                .orElseThrow(() -> new CustomException("Account has not been found"));
        account.setTitle(newTitle);
        Account updatedAccount = accountRepository.save(account);
        return accountToResponseConverter.convert(updatedAccount);
    }

    public AccountResponse findById(int id, int personId) {
        Account account = accountRepository.findAccountByIdAndPersonId(id, personId)
                .orElseThrow(() -> new CustomException("Account has not been found"));
        return accountToResponseConverter.convert(account);
    }
}
