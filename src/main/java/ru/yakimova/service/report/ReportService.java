package ru.yakimova.service.report;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yakimova.dao.report.ReportDao;

import java.time.LocalDateTime;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ReportService {

    private final ReportDao reportDao;

    public Map<String, Integer> findEarnTransactionToDate(
            int idPerson,
            LocalDateTime startDate,
            LocalDateTime endDate
    ) {
        return reportDao.findEarnTransactionToDate(idPerson, startDate, endDate);
    }

    public Map<String, Integer> findSpentTransactionToDate(
            int idPerson,
            LocalDateTime startDate,
            LocalDateTime endDate
    ) {
        return reportDao.findSpentTransactionToDate(idPerson, startDate, endDate);
    }
}
