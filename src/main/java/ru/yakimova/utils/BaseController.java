package ru.yakimova.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.yakimova.security.CustomUserDetails;

public class BaseController {

    public CustomUserDetails currentUser() {
        return (CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
    }
}
