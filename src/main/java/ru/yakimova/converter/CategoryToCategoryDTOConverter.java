package ru.yakimova.converter;

import org.springframework.stereotype.Service;
import ru.yakimova.dao.category.Category;
import ru.yakimova.service.category.CategoryDTO;

@Service
public class CategoryToCategoryDTOConverter implements Converter<Category, CategoryDTO> {

    @Override
    public CategoryDTO convert(Category source) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(source.getId());
        categoryDTO.setTitle(source.getTitle());
        categoryDTO.setPersonId(source.getPerson().getId());
        return categoryDTO;
    }
}
