package ru.yakimova.converter;

import org.springframework.stereotype.Service;
import ru.yakimova.dao.account.Account;
import ru.yakimova.service.account.AccountDTO;

@Service
public class AccountToAccountDTOConverter implements Converter<Account, AccountDTO> {

    @Override
    public AccountDTO convert(Account source) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(source.getId());
        accountDTO.setTitle(source.getTitle());
        accountDTO.setAmountMoney(source.getAmountMoney());
        accountDTO.setPersonId(source.getPerson().getId());
        return accountDTO;
    }
}
