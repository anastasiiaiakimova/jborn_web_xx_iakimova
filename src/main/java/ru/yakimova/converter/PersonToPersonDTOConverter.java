package ru.yakimova.converter;

import org.springframework.stereotype.Service;
import ru.yakimova.dao.person.Person;
import ru.yakimova.service.person.PersonDTO;

@Service
public class PersonToPersonDTOConverter implements Converter<Person, PersonDTO> {

    @Override
    public PersonDTO convert(Person source) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setId(source.getId());
        personDTO.setEmail(source.getEmail());
        return personDTO;
    }
}
