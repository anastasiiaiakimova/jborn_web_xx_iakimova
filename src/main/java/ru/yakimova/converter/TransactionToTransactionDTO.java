package ru.yakimova.converter;

import org.springframework.stereotype.Service;
import ru.yakimova.dao.transaction.Transaction;
import ru.yakimova.service.transaction.TransactionDTO;

@Service
public class TransactionToTransactionDTO implements Converter<Transaction, TransactionDTO> {

    @Override
    public TransactionDTO convert(Transaction source) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setId(source.getId());
        transactionDTO.setToAccountId(source.getToAccount().getId());
        transactionDTO.setFromAccountId(source.getFromAccount().getId());
        transactionDTO.setTransactionSum(source.getTransactionSum());
        transactionDTO.setCreatedDate(source.getCreatedDate());
        return transactionDTO;
    }
}
