package ru.yakimova.dao.report;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yakimova.exception.CustomException;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ReportDao {
    private final DataSource dataSource;

    public Map<String, Integer> findEarnTransactionToDate(
            int idPerson,
            LocalDateTime startDate,
            LocalDateTime endDate
    ) {
        Map<String, Integer> report = new HashMap<>();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT c.title, sum(t.transaction_sum) as summa " +
                    "FROM transaction as t" +
                    "         JOIN transaction_to_category ttc on t.id = ttc.transaction_id " +
                    "         JOIN category c on c.id = ttc.category_id " +
                    "         JOIN account a on a.id = t.to_account_id " +
                    "WHERE c.person_id = ? AND a.person_id = ? " +
                    "  AND t.created_date BETWEEN ? AND ? " +
                    "GROUP BY c.title;"
            );
            ps.setInt(1, idPerson);
            ps.setInt(2, idPerson);
            ps.setTimestamp(3, Timestamp.valueOf(startDate));
            ps.setTimestamp(4, Timestamp.valueOf(endDate));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                report.put(rs.getString("title"), rs.getInt("summa"));
            }
            return report;
        } catch (SQLException e) {
            throw new CustomException(e);
        }
    }

    public Map<String, Integer> findSpentTransactionToDate(
            int idPerson,
            LocalDateTime startDate,
            LocalDateTime endDate
    ) {
        Map<String, Integer> report = new HashMap<>();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT c.title, sum(t.transaction_sum) as summa " +
                    "FROM transaction as t" +
                    "         JOIN transaction_to_category ttc on t.id = ttc.transaction_id " +
                    "         JOIN category c on c.id = ttc.category_id " +
                    "         JOIN account a on a.id = t.from_account_id " +
                    "WHERE c.person_id = ? AND a.person_id = ? " +
                    "  AND t.created_date BETWEEN ? AND ? " +
                    "GROUP BY c.title;"
            );
            ps.setInt(1, idPerson);
            ps.setInt(2, idPerson);
            ps.setTimestamp(3, Timestamp.valueOf(startDate));
            ps.setTimestamp(4, Timestamp.valueOf(endDate));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                report.put(rs.getString("title"), rs.getInt("summa"));
            }
            return report;
        } catch (SQLException e) {
            throw new CustomException(e);
        }
    }
}
