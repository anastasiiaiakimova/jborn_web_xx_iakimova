package ru.yakimova.dao.account;

import lombok.Data;
import ru.yakimova.dao.person.Person;
import ru.yakimova.dao.transaction.Transaction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "amount_money")
    private int amountMoney;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id")
    private Person person;

    @OneToMany(
            cascade = CascadeType.ALL, orphanRemoval = true,
            mappedBy = "toAccount"
    )
    private List<Transaction> transactionEarn = new ArrayList<>();

    @OneToMany(
            cascade = CascadeType.ALL, orphanRemoval = true,
            mappedBy = "fromAccount"
    )
    private List<Transaction> transactionSpend = new ArrayList<>();

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", amountMoney=" + amountMoney +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account that = (Account) o;
        return id == that.id && amountMoney == that.amountMoney &&
                person.getId() == that.person.getId() && Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, amountMoney, person.getId());
    }
}
