package ru.yakimova.dao.transaction;

import org.springframework.stereotype.Service;
import ru.yakimova.dao.account.Account;
import ru.yakimova.dao.category.Category;
import ru.yakimova.dao.person.Person;
import ru.yakimova.exception.CustomException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Transaction create(
            int personId,
            int toAccountId,
            int fromAccountId,
            int transactionSum,
            List<Integer> categoryId
    ) {
        try {
            Account accountTo = em.find(Account.class, toAccountId);
            if (accountTo == null) {
                throw new CustomException("AccountTo has not been found");
            }
            Account accountFrom = em.find(Account.class, fromAccountId);
            if (accountFrom == null) {
                throw new CustomException("AccountFrom has not been found");
            }
            Person personModel = em.find(Person.class, personId);
            List<Account> accounts = personModel.getAccounts();
            if (!(accounts.contains(accountTo))) {
                throw new CustomException("AccountTo has not had person");
            }
            if (!(accounts.contains(accountFrom))) {
                throw new CustomException("AccountFrom has not had person");
            }
            int balanceAccountFrom = accountFrom.getAmountMoney();
            if (balanceAccountFrom >= transactionSum) {
                accountFrom.setAmountMoney(balanceAccountFrom + (-transactionSum));
                accountTo.setAmountMoney(accountTo.getAmountMoney() + transactionSum);
            } else {
                throw new CustomException("Transaction Sum too much");
            }
            List<Integer> categories = personModel.getCategories().stream()
                    .map(Category::getId)
                    .collect(Collectors.toList());
            if (new HashSet<>(categories).containsAll(categoryId)) {
                Transaction transaction = new Transaction();
                transaction.setToAccount(accountTo);
                transaction.setFromAccount(accountFrom);
                transaction.setTransactionSum(transactionSum);
                List<Category> categoriesForTransaction = categoryId.stream()
                        .map(i -> em.find(Category.class, i))
                        .collect(Collectors.toList());
                if (categoriesForTransaction.isEmpty()) {
                    throw new CustomException("Categories have not been found");
                }
                transaction.setCategories(categoriesForTransaction);
                transaction.setCreatedDate(LocalDateTime.now());
                em.persist(transaction);
                return transaction;
            } else {
                throw new CustomException("Categories have not belonged person");
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
}
