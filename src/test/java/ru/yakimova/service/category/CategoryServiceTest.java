package ru.yakimova.service.category;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.yakimova.api.converter.CategoryToResponseConverter;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.api.repository.CategoryRepository;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.category.Category;
import ru.yakimova.dao.person.Person;
import ru.yakimova.exception.CustomException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @InjectMocks
    CategoryService categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    PersonRepository personRepository;

    @Mock
    CategoryToResponseConverter converter;
    AutoCloseable autoCloseable;

    @BeforeEach
    public void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        categoryService = new CategoryService(categoryRepository, converter, personRepository);
    }

    @AfterEach
    public void after() throws Exception {
        autoCloseable.close();
    }

    @Test
    public void notCreate() {
        when(categoryRepository.findCategoryByIdAndPersonId(anyInt(), anyInt()))
                .thenThrow(new CustomException("Category has not been found"));

        CustomException exception = Assertions.assertThrows(CustomException.class, () -> {
            categoryService.update(1, "categoryName", 1);
        }, "Category has not been found");

        Assertions.assertEquals("Category has not been found", exception.getMessage());
    }

    @Test
    public void createSuccess() {
        Person person = new Person();
        person.setId(2);

        Category categoryMock = new Category();
        categoryMock.setId(1);
        categoryMock.setTitle("categoryName");
        categoryMock.setPerson(person);

        when(personRepository.findById(2)).thenReturn(Optional.of(person));

        when(categoryRepository.save(any(Category.class))).thenReturn(categoryMock);

        CategoryResponse categoryResponse = new CategoryResponse(1, "categoryName", 2);

        when(converter.convert(any(Category.class))).thenReturn(categoryResponse);

        CategoryResponse category = categoryService.create("categoryName", person.getId());

        assertNotNull(category);
        assertEquals(categoryResponse, category);
        verify(categoryRepository, times(1))
                .save(any(Category.class));
        verify(converter, times(1))
                .convert(any(Category.class));
    }

    @Test
    public void notEdit() {

        when(categoryRepository.findCategoryByIdAndPersonId(anyInt(), anyInt()))
                .thenThrow(new CustomException("Category has not been found"));

        CustomException exception = Assertions.assertThrows(CustomException.class, () -> {
            categoryService.update(1, "categoryName", 1);
        }, "Category has not been found");

        Assertions.assertEquals("Category has not been found", exception.getMessage());
    }

    @Test
    public void editSuccess() {
        Person person = new Person();
        person.setId(2);
        Category categoryMock = new Category();
        categoryMock.setId(1);
        categoryMock.setTitle("categoryName");
        categoryMock.setPerson(person);

        when(categoryRepository.findCategoryByIdAndPersonId(anyInt(), anyInt()))
                .thenReturn(Optional.of(categoryMock));

        CategoryResponse response = new CategoryResponse(1, "categoryName", 2);

        when(converter.convert(categoryMock)).thenReturn(response);

        CategoryResponse category = categoryService.update(1, "categoryName", 2);

        assertNotNull(category);
        assertEquals(response, category);
        verify(categoryRepository, times(1))
                .findCategoryByIdAndPersonId(anyInt(), anyInt());
        verify(converter, times(1))
                .convert(categoryMock);
    }

    @Test
    public void delete() {
        Person person = new Person();
        person.setId(2);
        Category categoryMock = new Category();
        categoryMock.setId(1);
        categoryMock.setTitle("categoryName");
        categoryMock.setPerson(person);

        when(categoryRepository.findCategoryByIdAndPersonId(1, 2))
                .thenReturn(Optional.of(categoryMock));

        categoryService.delete(1, 2);

        verify(categoryRepository, times(1))
                .delete(categoryMock);
        verifyNoInteractions(converter);
    }

    @Test
    public void findAllByPersonId() {
        Person person = new Person();
        person.setId(1);

        Category categoryModel1 = new Category();
        categoryModel1.setId(1);
        categoryModel1.setTitle("categoryName1");
        categoryModel1.setPerson(person);

        Category categoryModel2 = new Category();
        categoryModel2.setId(2);
        categoryModel2.setTitle("categoryName2");
        categoryModel2.setPerson(person);
        List<Category> categoryModels = Arrays.asList(categoryModel1, categoryModel2);

        when(categoryRepository.findCategoriesByPersonId(2))
                .thenReturn(categoryModels);

        CategoryResponse category1 = new CategoryResponse(1, "categoryName1", 1);

        CategoryResponse category2 = new CategoryResponse(1, "categoryName2", 1);

        List<CategoryResponse> categoryDTOS = Arrays.asList(category1, category2);

        when(converter.convert(categoryModel1))
                .thenReturn(category1);

        when(converter.convert(categoryModel2))
                .thenReturn(category2);

        List<CategoryResponse> categories = categoryService.findAllByPersonId(2);

        assertFalse(categories.isEmpty());
        assertEquals(categoryDTOS, categories);
        assertEquals(category1, categories.get(0));
        assertEquals(category2, categories.get(1));
        verify(categoryRepository, times(1))
                .findCategoriesByPersonId(2);
        verify(converter, times(1))
                .convert(categoryModel1);
        verify(converter, times(1))
                .convert(categoryModel2);
    }
}