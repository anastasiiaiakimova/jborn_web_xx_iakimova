package ru.yakimova.service.transaction;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.yakimova.api.converter.TransactionToResponseConverter;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.api.repository.AccountRepository;
import ru.yakimova.api.repository.CategoryRepository;
import ru.yakimova.api.repository.TransactionRepository;
import ru.yakimova.dao.account.Account;
import ru.yakimova.dao.category.Category;
import ru.yakimova.dao.person.Person;
import ru.yakimova.dao.transaction.Transaction;
import ru.yakimova.exception.CustomException;

import java.time.LocalDateTime;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @InjectMocks
    TransactionService transactionService;

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    AccountRepository accountRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    TransactionToResponseConverter converter;
    AutoCloseable autoCloseable;

    @BeforeEach
    public void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        transactionService = new TransactionService(
                accountRepository,
                categoryRepository,
                transactionRepository,
                converter
        );
    }

    @AfterEach
    public void after() throws Exception {
        autoCloseable.close();
    }

    @Test
    public void create() {
        Person person = new Person();
        person.setId(2);

        Account accountMock = new Account();
        accountMock.setId(1);
        accountMock.setTitle("newAccountName");
        accountMock.setPerson(person);
        accountMock.setAmountMoney(1234);

        when(accountRepository.findAccountByIdAndPersonId(eq(1), anyInt()))
                .thenReturn(Optional.of(accountMock));

        Account accountMock2 = new Account();
        accountMock2.setId(2);
        accountMock2.setTitle("newAccountName2");
        accountMock2.setPerson(person);
        accountMock2.setAmountMoney(2223);

        when(accountRepository.findAccountByIdAndPersonId(eq(2), anyInt()))
                .thenReturn(Optional.of(accountMock2));

        Category categoryMock = new Category();
        categoryMock.setId(1);
        categoryMock.setTitle("categoryName");
        categoryMock.setPerson(person);

        when(categoryRepository.findCategoryByIdAndPersonId(anyInt(), anyInt()))
                .thenReturn(Optional.of(categoryMock));

        Transaction transactionMock = new Transaction();
        transactionMock.setId(1);
        transactionMock.setTransactionSum(300);

        when(transactionRepository.save(any(Transaction.class)))
                .thenReturn(transactionMock);

        TransactionResponse response = new TransactionResponse(1, 1, 2, 300, LocalDateTime.now());

        when(converter.convert(any(Transaction.class))).thenReturn(response);

        TransactionResponse transaction = transactionService.create(1, 1, 2, 300, singletonList(1));

        assertNotNull(transaction);
        assertEquals(response, transaction);
        verify(transactionRepository, times(1))
                .save(any(Transaction.class));
        verify(converter, times(1))
                .convert(any(Transaction.class));
    }

    @Test
    public void notCreate() {
        Person person = new Person();
        person.setId(2);

        Account accountMock = new Account();
        accountMock.setId(1);
        accountMock.setTitle("newAccountName");
        accountMock.setPerson(person);
        accountMock.setAmountMoney(1234);

        when(accountRepository.findAccountByIdAndPersonId(eq(1), anyInt()))
                .thenReturn(Optional.of(accountMock));

        Account accountMock2 = new Account();
        accountMock2.setId(2);
        accountMock2.setTitle("newAccountName2");
        accountMock2.setPerson(person);
        accountMock2.setAmountMoney(23);

        when(accountRepository.findAccountByIdAndPersonId(eq(2), anyInt()))
                .thenReturn(Optional.of(accountMock2));

        Category categoryMock = new Category();
        categoryMock.setId(1);
        categoryMock.setTitle("categoryName");
        categoryMock.setPerson(person);

        when(categoryRepository.findCategoryByIdAndPersonId(anyInt(), anyInt()))
                .thenReturn(Optional.of(categoryMock));

        when(transactionRepository.save(any(Transaction.class)))
                .thenThrow(new CustomException("Transaction has not been created"));

        CustomException exception = Assertions.assertThrows(CustomException.class, () -> {
            transactionService.create(1, 2, 1, 300, singletonList(2));
        }, "Transaction has not been created");

        Assertions.assertEquals("Transaction has not been created", exception.getMessage());
    }
}