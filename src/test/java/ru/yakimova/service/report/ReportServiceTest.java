package ru.yakimova.service.report;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.yakimova.dao.report.ReportDao;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ReportServiceTest {

    @InjectMocks
    ReportService reportService;

    @Mock
    ReportDao reportDao;

    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    static LocalDateTime start = LocalDateTime.parse("2023-03-01 15:29:21", formatter);
    static LocalDateTime end = LocalDateTime.parse("2023-03-29 15:29:21", formatter);

    @Test
    public void findEarnTransactionToDate() {
        Map<String, Integer> reportMok = new HashMap<>();
        reportMok.put("CategoryOne", 300);
        reportMok.put("CategoryTwo", 130);
        reportMok.put("CategoryThree", 49);

        when(reportDao
                .findEarnTransactionToDate(1, start, end))
                .thenReturn(reportMok);

        Map<String, Integer> report = reportService.findEarnTransactionToDate(1, start, end);

        assertFalse(report.isEmpty());
        assertEquals(reportMok, report);
        verify(reportDao, times(1)).
                findEarnTransactionToDate(
                        1, start, end);
    }

    @Test
    public void notFindEarnTransactionToDate() {
        when(reportDao
                .findEarnTransactionToDate(1, start, end))
                .thenReturn(null);

        Map<String, Integer> report = reportService.findEarnTransactionToDate(1, start, end);

        assertNull(report);
        verify(reportDao, times(1)).
                findEarnTransactionToDate(1, start, end);
    }

    @Test
    public void findSpentTransactionToDate() {
        Map<String, Integer> reportMok = new HashMap<>();
        reportMok.put("CategoryOne", 300);
        reportMok.put("CategoryTwo", 130);
        reportMok.put("CategoryThree", 49);

        when(reportDao
                .findSpentTransactionToDate(1, start, end))
                .thenReturn(reportMok);

        Map<String, Integer> report = reportService.findSpentTransactionToDate(1, start, end);

        assertFalse(report.isEmpty());
        assertEquals(reportMok, report);
        verify(reportDao, times(1)).
                findSpentTransactionToDate(
                        1
                        , start
                        , end);
    }

    @Test
    public void notFindSpentTransactionToDate() {
        when(reportDao
                .findSpentTransactionToDate(1, start, end))
                .thenReturn(null);

        Map<String, Integer> report = reportService.findSpentTransactionToDate(1, start, end);

        assertNull(report);
        verify(reportDao, times(1)).
                findSpentTransactionToDate(1, start, end);
    }
}