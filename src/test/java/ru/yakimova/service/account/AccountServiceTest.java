package ru.yakimova.service.account;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.yakimova.api.converter.AccountToResponseConverter;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.api.repository.AccountRepository;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.account.Account;
import ru.yakimova.dao.person.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @InjectMocks
    AccountService accountService;

    @Mock
    AccountRepository accountRepository;

    @Mock
    PersonRepository personRepository;

    @Mock
    AccountToResponseConverter accountToResponseConverter;
    AutoCloseable autoCloseable;

    @BeforeEach
    public void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        accountService = new AccountService(accountRepository, personRepository, accountToResponseConverter);
    }

    @AfterEach
    public void after() throws Exception {
        autoCloseable.close();
    }

    @Test
    public void notFindAllByPersonId() {
        when(accountRepository.findAccountByPersonId(1))
                .thenReturn(new ArrayList<>());

        List<AccountResponse> allAccounts = accountService.findAllByPersonId(1);

        Assertions.assertEquals(new ArrayList<>(), allAccounts);
        verify(accountRepository, times(1))
                .findAccountByPersonId(1);
        verifyNoInteractions(accountToResponseConverter);
    }

    @Test
    public void findAllByPersonIdSuccess() {
        Person person = new Person();
        person.setId(2);

        Account account1 = new Account();
        account1.setId(1);
        account1.setTitle("accountName");
        account1.setPerson(person);

        Account account2 = new Account();
        account2.setId(2);
        account2.setTitle("accountName");
        account2.setPerson(person);
        List<Account> models = Arrays.asList(account1, account2);

        when(accountRepository.findAccountByPersonId(2))
                .thenReturn(models);

        AccountResponse accountResponse1 = new AccountResponse(1, "accountName", 222, 2);
        AccountResponse accountResponse2 = new AccountResponse(2, "accountName2", 300, 2);
        List<AccountResponse> responses = Arrays.asList(accountResponse1, accountResponse2);

        when(accountToResponseConverter.convert(account1))
                .thenReturn(accountResponse1);

        when(accountToResponseConverter.convert(account2))
                .thenReturn(accountResponse2);

        List<AccountResponse> accounts = accountService.findAllByPersonId(2);

        assertFalse(accounts.isEmpty());
        assertEquals(responses, accounts);
        assertEquals(accountResponse1, accounts.get(0));
        assertEquals(accountResponse2, accounts.get(1));
        verify(accountRepository, times(1))
                .findAccountByPersonId(2);
        verify(accountToResponseConverter, times(1))
                .convert(account1);
        verify(accountToResponseConverter, times(1))
                .convert(account2);
    }

    @Test
    public void notCreate() {
        Person person = new Person();
        person.setId(1);

        when(personRepository.findById(1)).thenReturn(Optional.of(person));

        when(accountRepository.save(any(Account.class)))
                .thenReturn(null);

        AccountResponse accountResponse = accountService.create(1, "accountName", 100);

        assertNull(accountResponse);
        verify(accountRepository, times(1))
                .save(any(Account.class));
        verify(accountToResponseConverter, times(1))
                .convert(null);
    }

    @Test
    public void createSuccess() {
        Person person = new Person();
        person.setId(2);

        Account accountMock = new Account();
        accountMock.setId(1);
        accountMock.setTitle("accountName");
        accountMock.setPerson(person);

        when(personRepository.findById(2)).thenReturn(Optional.of(person));

        when(accountRepository.save(any(Account.class))).thenReturn(accountMock);

        AccountResponse accountResponse = new AccountResponse(1, "accountName", 800, 2);

        when(accountToResponseConverter.convert(accountMock)).thenReturn(accountResponse);

        AccountResponse account = accountService.create(person.getId(), "accountName", 800);

        assertNotNull(account);
        assertEquals(accountResponse, account);
        verify(accountRepository, times(1))
                .save(any(Account.class));
        verify(accountToResponseConverter, times(1))
                .convert(accountMock);
    }

    @Test
    public void updateSuccess() {
        Person person = new Person();
        person.setId(2);

        Account accountMock = new Account();
        accountMock.setId(1);
        accountMock.setTitle("newAccountName");
        accountMock.setPerson(person);

        when(accountRepository.findAccountByIdAndPersonId(anyInt(), anyInt()))
                .thenReturn(Optional.of(accountMock));

        when(accountRepository.save(any(Account.class))).thenReturn(accountMock);

        AccountResponse accountResponse = new AccountResponse(1, "newAccountName", 200, 2);

        when(accountToResponseConverter.convert(accountMock)).thenReturn(accountResponse);

        AccountResponse account = accountService.update(2, 1, "newAccountName");

        assertNotNull(account);
        assertEquals(accountResponse, account);
        verify(accountRepository, times(1))
                .save(any(Account.class));
        verify(accountToResponseConverter, times(1))
                .convert(accountMock);
    }

    @Test
    public void delete() {
        Person person = new Person();
        person.setId(2);

        Account accountMock = new Account();
        accountMock.setId(1);
        accountMock.setTitle("newAccountName");
        accountMock.setPerson(person);

        when(accountRepository.findAccountByIdAndPersonId(anyInt(), anyInt()))
                .thenReturn(Optional.of(accountMock));
        accountService.delete(1, 2);

        verify(accountRepository, times(1))
                .delete(accountMock);
        verifyNoInteractions(accountToResponseConverter);
    }
}