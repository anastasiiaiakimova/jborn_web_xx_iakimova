package ru.yakimova.service.person;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.yakimova.api.converter.PersonToResponseConverter;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.api.repository.PersonRepository;
import ru.yakimova.dao.person.Person;
import ru.yakimova.exception.CustomException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {

    @InjectMocks
    AuthService authService;

    @Mock
    PersonRepository personRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    PersonToResponseConverter personToResponseConverter;
    AutoCloseable autoCloseable;

    @BeforeEach
    public void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        authService = new AuthService(personRepository, passwordEncoder, personToResponseConverter);
    }

    @AfterEach
    public void after() throws Exception {
        autoCloseable.close();
    }

    @Test
    public void authPersonNotFound() {
        when(personRepository.findByEmail(anyString()))
                .thenThrow(new CustomException("Person has not been found"));

        CustomException exception = Assertions.assertThrows(CustomException.class, () -> {
            authService.auth("nastya", "12345");
        }, "Person has not been found");

        Assertions.assertEquals("Person has not been found", exception.getMessage());
    }

    @Test
    public void authPersonFound() {
        when(passwordEncoder.matches(anyString(), eq("hex"))).thenReturn(true);

        Person personMock = new Person();
        personMock.setId(1);
        personMock.setEmail("yakimova828@gmail.com");
        personMock.setPassword("hex");

        when(personRepository.findById(anyInt())).thenReturn(Optional.of(personMock));

        when(personRepository.findByEmail(anyString())).thenReturn(Optional.of(personMock));

        AuthResponse response = new AuthResponse(1, "yakimova828@gmail.gmail");

        when(personToResponseConverter.convert(any(Person.class))).thenReturn(response);

        AuthResponse person = authService.auth("yakimova828@gmail.com", "hex");

        assertNotNull(person);
        assertEquals(response, person);
        verify(passwordEncoder, times(1))
                .matches("hex", "hex");
        verify(personRepository, times(1))
                .findById(anyInt());
        verify(personToResponseConverter, times(2))
                .convert(personMock);
    }

    @Test
    public void registrationNotSuccess() {
        when(personRepository.save(any(Person.class)))
                .thenThrow(new CustomException("Person has not been registered"));

        CustomException exception = Assertions.assertThrows(CustomException.class, () -> {
            authService.registration("nastya", "12345");
        }, "Person has not been registered");

        Assertions.assertEquals("Person has not been registered", exception.getMessage());
    }

    @Test
    public void registrationSuccess() {
        when(passwordEncoder.encode("12345")).thenReturn("hex");

        Person personMock = new Person();
        personMock.setId(1);
        personMock.setEmail("yakimova828@gmail.com");
        personMock.setPassword("hex");

        when(personRepository.save(any(Person.class))).thenReturn(personMock);

        AuthResponse response = new AuthResponse(1, "nastya");

        when(personToResponseConverter.convert(any(Person.class))).thenReturn(response);

        AuthResponse person = authService.registration("nastya", "12345");

        assertNotNull(person);
        assertEquals(response, person);
        verify(passwordEncoder, times(1)).encode("12345");
        verify(personRepository, times(1)).save(any(Person.class));
        verify(personToResponseConverter, times(1)).convert(any(Person.class));
    }
}