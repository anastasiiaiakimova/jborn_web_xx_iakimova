package ru.yakimova;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.yakimova.security.CustomGrantedAuthority;
import ru.yakimova.security.CustomUserDetailService;
import ru.yakimova.security.CustomUserDetails;

import static java.util.Collections.singleton;
import static ru.yakimova.security.UserRole.USER;

@Configuration
public class MockSecurityConfiguration {
    @Bean
    public CustomUserDetailService userDetailService() {
        return new CustomUserDetailService(null) {
            @Override
            public CustomUserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
                return new CustomUserDetails(
                        1,
                        "redact@gmail.com",
                        "password",
                        singleton(new CustomGrantedAuthority(USER))
                );
            }
        };
    }
}
