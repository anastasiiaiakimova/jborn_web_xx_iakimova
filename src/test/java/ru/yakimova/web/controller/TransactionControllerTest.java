package ru.yakimova.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.service.transaction.TransactionService;
import ru.yakimova.web.form.TransactionForm;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TransactionController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class TransactionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TransactionService transactionService;

    @MockBean
    AccountService accountService;

    @MockBean
    CategoryService categoryService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void createForm() throws Exception {
        List<AccountResponse> accounts = asList(
                new AccountResponse(1, "sber", 200, 1),
                new AccountResponse(2, "tink", 290, 1),
                new AccountResponse(4, "pochta", 1200, 1)
        );

        when(accountService.findAllByPersonId(1)).thenReturn(accounts);

        List<CategoryResponse> categories = asList(
                new CategoryResponse(1, "salary", 1),
                new CategoryResponse(2, "shop", 1),
                new CategoryResponse(4, "dog", 1)
        );

        when(categoryService.findAllByPersonId(1)).thenReturn(categories);

        mockMvc.perform(get("/transaction/create-transaction"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("accounts", accounts))
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attribute("form", new TransactionForm()))
                .andExpect(view().name("/transaction/create-transaction"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void createTransaction() throws Exception {
        TransactionResponse transactional = new TransactionResponse(1, 2, 5, 230, LocalDateTime.now());
        when(transactionService.create(1, 2, 5, 230, singletonList(1)))
                .thenReturn(transactional);

        AccountResponse accountFrom = new AccountResponse(5, "tink", 300, 1);
        when(accountService.findById(5, 1))
                .thenReturn(accountFrom);

        AccountResponse accountTo = new AccountResponse(2, "sber", 700, 1);
        when(accountService.findById(2, 1))
                .thenReturn(accountTo);

        CategoryResponse categoryResponse = new CategoryResponse(1, "salary", 1);
        when(categoryService.findById(1, 1))
                .thenReturn(categoryResponse);

        mockMvc.perform(post("/transaction/create-transaction")
                        .param("toAccountId", String.valueOf(2))
                        .param("fromAccountId", String.valueOf(5)).
                        param("transactionSum", String.valueOf(230))
                        .param("categoriesId", String.valueOf(1)))
                .andExpect(status().isOk())
                .andExpect(model().attribute("transactional", transactional))
                .andExpect(model().attribute("accountFrom", accountFrom.getTitle()))
                .andExpect(model().attribute("accountTo", accountTo.getTitle()))
                .andExpect(model().attribute("categories", singletonList(categoryResponse)))
                .andExpect(view().name("/transaction/transaction-show"));
    }
}