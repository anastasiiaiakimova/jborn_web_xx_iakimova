package ru.yakimova.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.web.form.AccountForm;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AccountController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class AccountControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AccountService accountService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void index() throws Exception {
        List<AccountResponse> accounts = Arrays.asList(
                new AccountResponse(1, "sber", 200, 1),
                new AccountResponse(2, "tink", 290, 1),
                new AccountResponse(4, "pochta", 1200, 1)
        );

        when(accountService.findAllByPersonId(1)).thenReturn(accounts);

        mockMvc.perform(get("/account/find-all-accounts"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("accounts", accounts))
                .andExpect(view().name("/account/accounts"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void createForm() throws Exception {
        mockMvc.perform(get("/account/create-account"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new AccountForm()))
                .andExpect(view().name("/account/create-account"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void reportRequest() throws Exception {
        when(accountService.create(1, "terra1", 300))
                .thenReturn(new AccountResponse(2, "terra1", 300, 1));

        mockMvc.perform(post("/account/create-account")
                        .param("id", String.valueOf(2))
                        .param("title", "terra1")
                        .param("amountMoney", String.valueOf(300)))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/account/find-all-accounts"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void getAccountById() throws Exception {
        when(accountService.findById(2, 1))
                .thenReturn(new AccountResponse(2, "terra1", 300, 1));

        mockMvc.perform(get("/account/2"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("account", new AccountResponse(2, "terra1", 300, 1)))
                .andExpect(view().name("/account/accountId"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void showEditAccount() throws Exception {
        when(accountService.findById(2, 1))
                .thenReturn(new AccountResponse(2, "terra1", 300, 1));

        mockMvc.perform(get("/account/2/edit"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("account", new AccountResponse(2, "terra1", 300, 1)))
                .andExpect(view().name("/account/edit-account"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void editAccount() throws Exception {
        when(accountService.update(1, 2, "newTitle"))
                .thenReturn(new AccountResponse(2, "newTitle", 120, 1));

        mockMvc.perform(post("/account/2/edit")
                        .param("title", "newTitle"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/account/find-all-accounts"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void deleteAccount() throws Exception {
        mockMvc.perform(get("/account/2/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/account/find-all-accounts"));
    }
}