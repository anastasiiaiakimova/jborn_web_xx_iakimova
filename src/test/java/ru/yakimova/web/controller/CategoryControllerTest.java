package ru.yakimova.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.web.form.CategoryForm;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CategoryController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class CategoryControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CategoryService categoryService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void index() throws Exception {
        List<CategoryResponse> categories = Arrays.asList(
                new CategoryResponse(1, "salary", 1),
                new CategoryResponse(2, "shop", 1),
                new CategoryResponse(4, "dog", 1)
        );

        when(categoryService.findAllByPersonId(1)).thenReturn(categories);

        mockMvc.perform(get("/category/find-all-categories"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(view().name("/category/categories"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void createForm() throws Exception {
        mockMvc.perform(get("/category/create-category"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new CategoryForm()))
                .andExpect(view().name("/category/create-category"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void reportRequest() throws Exception {
        when(categoryService.create("title", 1))
                .thenReturn(new CategoryResponse(2, "title", 1));

        mockMvc.perform(post("/category/create-category")
                        .param("id", String.valueOf(2))
                        .param("title", "title"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/category/find-all-categories"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void getCategoryById() throws Exception {
        when(categoryService.findById(2, 1))
                .thenReturn(new CategoryResponse(2, "terra1", 1));

        mockMvc.perform(get("/category/2"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("category", new CategoryResponse(2, "terra1", 1)))
                .andExpect(view().name("/category/categoryId"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void showEditCategory() throws Exception {
        when(categoryService.findById(2, 1))
                .thenReturn(new CategoryResponse(2, "category1", 1));

        mockMvc.perform(get("/category/2/edit"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("category", new CategoryResponse(2, "category1", 1)))
                .andExpect(view().name("/category/edit-category"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void editCategory() throws Exception {
        when(categoryService.update(1, "newTitle", 1))
                .thenReturn(new CategoryResponse(2, "newTitle", 1));

        mockMvc.perform(post("/category/2/edit")
                        .param("title", "newTitle"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/category/find-all-categories"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void deleteCategory() throws Exception {
        mockMvc.perform(get("/category/2/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/category/find-all-categories"));
    }
}