package ru.yakimova.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.report.ReportService;
import ru.yakimova.web.form.ReportForm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ReportController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class ReportControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ReportService reportService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void reportRequest() throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.parse("2023-02-01 12:00:00", formatter);
        LocalDateTime end = LocalDateTime.parse("2024-01-04 12:00:00", formatter);

        Map<String, Integer> reportEarn = new HashMap<>(Collections.singletonMap("cat1", 100));

        when(reportService.findEarnTransactionToDate(1, start, end))
                .thenReturn(reportEarn);

        Map<String, Integer> reportSpent = new HashMap<>(Collections.singletonMap("cat2", 200));

        when(reportService.findSpentTransactionToDate(1, start, end))
                .thenReturn(reportSpent);

        mockMvc.perform(post("/report/create-report")
                        .param("startDate", "2023-02-01 12:00:00")
                        .param("endDate", "2024-01-04 12:00:00"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("reportEarn", reportEarn))
                .andExpect(model().attribute("reportSpent", reportSpent))
                .andExpect(view().name("/report/all-report"));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void reportDate() throws Exception {
        mockMvc.perform(get("/report/create-report"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new ReportForm()))
                .andExpect(view().name("/report/create-report"));
    }
}