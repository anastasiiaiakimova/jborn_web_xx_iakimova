package ru.yakimova.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.person.AuthService;
import ru.yakimova.web.form.LoginForm;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LoginController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class LoginControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthService authService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void index() throws Exception {
        when(authService.getPersonById(1))
                .thenReturn(new AuthResponse(1, "redact@gmail.com"));

        mockMvc.perform(get("/person/personal-area"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("id", 1))
                .andExpect(model().attribute("email", "redact@gmail.com"))
                .andExpect(view().name("/person/personal-area"));
    }

    @Test
    public void getLogin() throws Exception {
        mockMvc.perform(get("/person/login-form"))
                .andExpect(status().isOk())
                .andExpect(view().name("/person/login-form"));
    }

    @Test
    public void getRegistration() throws Exception {
        mockMvc.perform(get("/person/registration"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new LoginForm()))
                .andExpect(view().name("/person/registration"));
    }

    @Test
    public void postRegistration() throws Exception {
        when(authService.registration("nastya@gmail.com", "password"))
                .thenReturn(new AuthResponse(1, "nastya@gmail.com"));

        mockMvc.perform(post("/person/registration")
                        .param("email", "nastya@gmail.com")
                        .param("password", "password"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/person/login"));
    }
}