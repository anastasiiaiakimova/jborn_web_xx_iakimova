package ru.yakimova.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static ru.yakimova.security.UserRole.USER;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CustomUserDetailServiceTest {

    @Autowired
    CustomUserDetailService subj;

    @Test
    public void loadUserByUsername() {
        UserDetails userDetails = subj.loadUserByUsername("redact@gmail.com");

        assertNotNull(userDetails);
        assertEquals("redact@gmail.com", userDetails.getUsername());
        assertEquals("password", userDetails.getPassword());
        assertEquals(1, userDetails.getAuthorities().size());
        assertEquals(new CustomGrantedAuthority(USER), userDetails.getAuthorities().iterator().next());
    }
}