package ru.yakimova.api.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.yakimova.dao.account.Account;

import java.util.List;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class AccountRepositoryTest {

    @Autowired
    AccountRepository subj;

    @Test
    public void findAccountByPersonId() {
        List<Account> accounts = subj.findAccountByPersonId(1);

        assertFalse(accounts.isEmpty());
        assertEquals(2, accounts.size());
        assertEquals(3, accounts.get(0).getId());
        assertEquals("alfa", accounts.get(0).getTitle());
        assertEquals(700, accounts.get(0).getAmountMoney());
        assertEquals(1, accounts.get(0).getPerson().getId());

        assertEquals(5, accounts.get(1).getId());
        assertEquals("pochta", accounts.get(1).getTitle());
        assertEquals(700, accounts.get(1).getAmountMoney());
        assertEquals(1, accounts.get(1).getPerson().getId());
    }

    @Test
    public void notFindAccountByPersonId() {
        List<Account> accounts = subj.findAccountByPersonId(5);

        assertTrue(accounts.isEmpty());
    }

    @Test
    public void findAccountByIdAndPersonId() {
        Account account = subj.findAccountByIdAndPersonId(3, 1).orElse(null);

        assertNotNull(account);
        assertEquals(3, account.getId());
        assertEquals("alfa", account.getTitle());
        assertEquals(700, account.getAmountMoney());
        assertEquals(1, account.getPerson().getId());
    }

    @Test
    public void notFindAccountByThisIdAndPersonId() {
        Account account = subj.findAccountByIdAndPersonId(10, 1).orElse(null);

        assertNull(account);
    }
    @Test
    public void notFindAccountByIdAndThisPersonId() {
        Account account = subj.findAccountByIdAndPersonId(3, 2).orElse(null);

        assertNull(account);
    }
}