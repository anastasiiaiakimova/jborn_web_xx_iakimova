package ru.yakimova.api.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.yakimova.dao.category.Category;

import java.util.List;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class CategoryRepositoryTest {

    @Autowired
    CategoryRepository subj;

    @Test
    public void findCategoriesByPersonId() {
        List<Category> categories = subj.findCategoriesByPersonId(2);

        assertFalse(categories.isEmpty());
        assertEquals(3, categories.size());

        assertEquals(3, categories.get(0).getId());
        assertEquals("flat", categories.get(0).getTitle());
        assertEquals(2, categories.get(0).getPerson().getId());

        assertEquals(2, categories.get(1).getId());
        assertEquals("salary", categories.get(1).getTitle());
        assertEquals(2, categories.get(1).getPerson().getId());

        assertEquals(4, categories.get(2).getId());
        assertEquals("salary1", categories.get(2).getTitle());
        assertEquals(2, categories.get(2).getPerson().getId());
    }

    @Test
    public void notFindCategoriesByThisPersonId() {
        List<Category> categories = subj.findCategoriesByPersonId(10);

        assertTrue(categories.isEmpty());
    }

    @Test
    public void findCategoryByIdAndPersonId() {
        Category category = subj.findCategoryByIdAndPersonId(3, 2).orElse(null);

        assertNotNull(category);
        assertEquals(3, category.getId());
        assertEquals("flat", category.getTitle());
        assertEquals(2, category.getPerson().getId());
    }

    @Test
    public void notFindCategoryByThisIdAndPersonId() {
        Category category = subj.findCategoryByIdAndPersonId(6, 2).orElse(null);

        assertNull(category);
    }

    @Test
    public void notFindCategoryByIdAndThisPersonId() {
        Category category = subj.findCategoryByIdAndPersonId(3, 5).orElse(null);

        assertNull(category);
    }
}