package ru.yakimova.api.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.yakimova.dao.person.Person;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@RunWith(SpringRunner.class)
public class PersonRepositoryTest {

    @Autowired
    PersonRepository subj;

    @Test
    public void findByEmail() {
        Person person = subj.findByEmail("redact@gmail.com").orElse(null);

        assertNotNull(person);
        assertEquals(1, person.getId());
        assertEquals("redact@gmail.com", person.getEmail());
        assertEquals("$2a$10$ljMIg3teP6VdItnQPd7LaOSe/KDeT7fqB.V6Q3QuDZG19iwJi0IRi", person.getPassword());
    }

    @Test
    public void notFindByThisEmail() {
        Person person = subj.findByEmail("redact@gmail.ru").orElse(null);

        assertNull(person);
    }

    @Test
    public void findByEmailAndPassword() {
        Person person = subj.findByEmailAndPassword(
                "inna@gmail.com",
                "$2a$10$ljMIg3teP6VdItnQPd7LaOSe/KDeT7fqB.V6Q3QuDZG19iwJi0IRi"
        );

        assertNotNull(person);
        assertEquals(2, person.getId());
        assertEquals("inna@gmail.com", person.getEmail());
        assertEquals("$2a$10$ljMIg3teP6VdItnQPd7LaOSe/KDeT7fqB.V6Q3QuDZG19iwJi0IRi", person.getPassword());
    }

    @Test
    public void notFindByEmailAndThisPassword() {
        Person person = subj.findByEmailAndPassword(
                "inna@gmail.com",
                "1"
        );
        assertNull(person);
    }

    @Test
    public void notFindByThisEmailAndPassword() {
        Person person = subj.findByEmailAndPassword(
                "inna@gmail.ru",
                "$2a$10$ljMIg3teP6VdItnQPd7LaOSe/KDeT7fqB.V6Q3QuDZG19iwJi0IRi"
        );
        assertNull(person);
    }
}