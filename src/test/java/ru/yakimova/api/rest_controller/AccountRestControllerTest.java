package ru.yakimova.api.rest_controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.account.*;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.account.AccountService;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AccountRestController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class AccountRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    AccountService accountService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void findAllById() throws Exception {
        List<AccountResponse> accounts = Arrays.asList(
                new AccountResponse(1, "sber", 200, 1),
                new AccountResponse(2, "tink", 290, 1),
                new AccountResponse(4, "pochta", 1200, 1)
        );

        when(accountService.findAllByPersonId(1)).thenReturn(accounts);

        mockMvc.perform(get("/api/find-all-accounts"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        objectMapper.writeValueAsString(new FindAllAccountsResponse(accounts))));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void create() throws Exception {
        AccountResponse accountResponse = new AccountResponse(2, "Account1", 300, 1);

        when(accountService.create(1, "Account1", 300))
                .thenReturn(accountResponse);

        AccountRequest accountRequest = new AccountRequest();
        accountRequest.setTitle("Account1");
        accountRequest.setAmountMoney(300);

        mockMvc.perform(post("/api/create-account")
                        .content(objectMapper.writeValueAsString(accountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(accountResponse)));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void update() throws Exception {
        AccountResponse accountResponse = new AccountResponse(2, "newTitle", 120, 1);

        when(accountService.update(1, 2, "newTitle"))
                .thenReturn(accountResponse);

        UpdateAccountRequest accountRequest = new UpdateAccountRequest();
        accountRequest.setId(2);
        accountRequest.setTitle("newTitle");

        mockMvc.perform(post("/api/update-account")
                        .content(objectMapper.writeValueAsString(accountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(accountResponse)));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void delete() throws Exception {
        doNothing().when(accountService).delete(1, 1);

        DeleteAccountRequest accountRequest = new DeleteAccountRequest();
        accountRequest.setId(1);

        mockMvc.perform(post("/api/delete-account")
                        .content(objectMapper.writeValueAsString(accountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(
                        new DeleteAccountResponse("Account " + accountRequest.getId() + " has been deleted"))));
    }
}