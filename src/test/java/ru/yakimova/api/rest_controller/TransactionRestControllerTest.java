package ru.yakimova.api.rest_controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.account.AccountResponse;
import ru.yakimova.api.json.account.DeleteAccountResponse;
import ru.yakimova.api.json.category.CategoryResponse;
import ru.yakimova.api.json.transaction.TransactionRequest;
import ru.yakimova.api.json.transaction.TransactionResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.account.AccountService;
import ru.yakimova.service.category.CategoryService;
import ru.yakimova.service.transaction.TransactionService;

import java.time.LocalDateTime;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TransactionRestController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class TransactionRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    TransactionService transactionService;

    @MockBean
    AccountService accountService;

    @MockBean
    CategoryService categoryService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void create() throws Exception {
        TransactionResponse transactional = new TransactionResponse(1, 2, 5, 230, LocalDateTime.now());
        when(transactionService.create(1, 2, 5, 230, singletonList(1)))
                .thenReturn(transactional);

        AccountResponse accountFrom = new AccountResponse(5, "tink", 300, 1);
        when(accountService.findById(5, 1))
                .thenReturn(accountFrom);

        AccountResponse accountTo = new AccountResponse(2, "sber", 700, 1);
        when(accountService.findById(2, 1))
                .thenReturn(accountTo);

        CategoryResponse categoryResponse = new CategoryResponse(1, "salary", 1);
        when(categoryService.findById(1, 1))
                .thenReturn(categoryResponse);

        TransactionRequest request = new TransactionRequest();
        request.setToAccountId(2);
        request.setFromAccountId(5);
        request.setTransactionSum(230);
        request.setCategoriesId(singletonList(1));

        mockMvc.perform(post("/api/create-transaction")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(transactional)));
    }
}