package ru.yakimova.api.rest_controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.report.AllReportResponse;
import ru.yakimova.api.json.report.ReportRequest;
import ru.yakimova.api.json.report.ReportResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.report.ReportService;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReportRestController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class ReportRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    ReportService reportService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void findAllReport() throws Exception {
        LocalDateTime start = LocalDateTime.now().minusDays(3);
        LocalDateTime end = LocalDateTime.now().plusDays(2);
        Map<String, Integer> reportEarn = new HashMap<>(Collections.singletonMap("cat1", 100));

        when(reportService.findEarnTransactionToDate(1, start, end))
                .thenReturn(reportEarn);

        Map<String, Integer> reportSpent = new HashMap<>(Collections.singletonMap("cat2", 200));

        when(reportService.findSpentTransactionToDate(1, start, end))
                .thenReturn(reportSpent);

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setStartDate(start);
        reportRequest.setEndDate(end);

        mockMvc.perform(post("/api/all-report")
                        .content(objectMapper.writeValueAsString(reportRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new AllReportResponse(reportEarn, reportSpent))));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void findEarnReport() throws Exception {
        LocalDateTime start = LocalDateTime.now().minusDays(3);
        LocalDateTime end = LocalDateTime.now().plusDays(2);
        Map<String, Integer> reportEarn = new HashMap<>(Collections.singletonMap("cat1", 100));

        when(reportService.findEarnTransactionToDate(1, start, end))
                .thenReturn(reportEarn);

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setStartDate(start);
        reportRequest.setEndDate(end);

        mockMvc.perform(post("/api/earn-report")
                        .content(objectMapper.writeValueAsString(reportRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new ReportResponse(reportEarn))));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void findSpentReport() throws Exception {
        LocalDateTime start = LocalDateTime.now().minusDays(3);
        LocalDateTime end = LocalDateTime.now().plusDays(2);
        Map<String, Integer> reportSpent = new HashMap<>(Collections.singletonMap("cat2", 200));

        when(reportService.findSpentTransactionToDate(1, start, end))
                .thenReturn(reportSpent);

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setStartDate(start);
        reportRequest.setEndDate(end);

        mockMvc.perform(post("/api/spent-report")
                        .content(objectMapper.writeValueAsString(reportRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new ReportResponse(reportSpent))));
    }
}