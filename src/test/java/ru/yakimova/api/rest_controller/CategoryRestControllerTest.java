package ru.yakimova.api.rest_controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.category.*;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.category.CategoryService;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CategoryRestController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class CategoryRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    CategoryService categoryService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void findAllCategoryResponse() throws Exception {
        List<CategoryResponse> categories = Arrays.asList(
                new CategoryResponse(1, "salary", 1),
                new CategoryResponse(2, "shop", 1),
                new CategoryResponse(4, "dog", 1)
        );
        when(categoryService.findAllByPersonId(1)).thenReturn(categories);

        mockMvc.perform(get("/api/find-all-categories"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        objectMapper.writeValueAsString(new FindAllCategoryResponse(categories))));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void create() throws Exception {
        CategoryResponse categoryResponse = new CategoryResponse(2, "Category1", 1);
        when(categoryService.create("Category1", 1))
                .thenReturn(categoryResponse);

        CategoryRequest categoryRequest = new CategoryRequest();
        categoryRequest.setTitle("Category1");

        mockMvc.perform(post("/api/create-category")
                        .content(objectMapper.writeValueAsString(categoryRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(categoryResponse)));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void update() throws Exception {
        CategoryResponse categoryResponse = new CategoryResponse(2, "newTitle", 1);

        when(categoryService.update(2, "newTitle", 1))
                .thenReturn(categoryResponse);

        UpdateCategoryRequest categoryRequest = new UpdateCategoryRequest();
        categoryRequest.setId(2);
        categoryRequest.setTitle("newTitle");

        mockMvc.perform(post("/api/update-category")
                        .content(objectMapper.writeValueAsString(categoryRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(categoryResponse)));
    }

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void delete() throws Exception {
        doNothing().when(categoryService).delete(2, 1);

        DeleteCategoryRequest categoryRequest = new DeleteCategoryRequest();
        categoryRequest.setId(2);

        mockMvc.perform(post("/api/delete-category")
                        .content(objectMapper.writeValueAsString(categoryRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(
                        new DeleteCategoryResponse("Category " + categoryRequest.getId() + " has been deleted"))));
    }
}