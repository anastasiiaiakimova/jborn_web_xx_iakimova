package ru.yakimova.api.rest_controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.yakimova.MockSecurityConfiguration;
import ru.yakimova.api.json.person.AuthRequest;
import ru.yakimova.api.json.person.AuthResponse;
import ru.yakimova.security.SecurityConfiguration;
import ru.yakimova.service.person.AuthService;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthRestController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class AuthRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    AuthService authService;

    @WithUserDetails(value = "redact@gmail.com", userDetailsServiceBeanName = "userDetailService")
    @Test
    public void login() throws Exception {
        AuthResponse authResponse = new AuthResponse(1, "redact@gmail.com");
        when(authService.getPersonById(1))
                .thenReturn(authResponse);

        mockMvc.perform(get("/api/person"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(authResponse)));
    }

    @Test
    public void registration() throws Exception {
        when(authService.registration("nastya@gmail.com", "password"))
                .thenReturn(new AuthResponse(1, "nastya@gmail.com"));

        AuthRequest person = new AuthRequest();
        person.setEmail("nastya@gmail.com");
        person.setPassword("password");

        mockMvc.perform(post("/api/registration")
                        .content(objectMapper.writeValueAsString(person))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}